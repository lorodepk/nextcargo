package com.ub.nextcargo;

import com.ub.nextcargo.application.CargoBookingService;
import com.ub.nextcargo.domen.*;
import com.ub.nextcargo.persistance.CargoRepository;
import com.ub.nextcargo.persistance.VoyageRepository;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
class NextcargoApplicationTests {
	@Autowired
	private CargoBookingService cargoBookingService;
	@Autowired
	private CargoRepository cargoRepository;
	@Autowired
	private VoyageRepository voyageRepository;

	@Test
	public void deleteRepos(){
		cargoRepository.deleteAll();
		voyageRepository.deleteAll();
	}

	@Test
	public void createCargo(){
		//Cargo cargo = new Cargo();
		//cargoRepository.save(cargo);
		for(Cargo cargo : cargoRepository.findAll()){
			System.out.println(cargo.getId());
		}
	}

	@Test
	public void testShortestPath(){
		//cargoBookingService.addSchedule(new Location("Volgograd"), new Location("Kiev"), LocalDateTime.of(2020,1,1,1,1,1),LocalDateTime.of(2020,1,2,1,1,1));
		//cargoBookingService.saveVoyage();
		cargoBookingService.generateItinerary(new Location("Volgograd"), new Location("Kiev"));
		for(CarrierMovement carrierMovement : cargoBookingService.getShortestPath()){
			System.out.println(carrierMovement.getDepartureLocation().getName());
			System.out.println(carrierMovement.getArriveLocation().getName());
		}
	}
}
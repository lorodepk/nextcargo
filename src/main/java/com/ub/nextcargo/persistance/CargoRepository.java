package com.ub.nextcargo.persistance;

import com.ub.nextcargo.domen.Cargo;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CargoRepository extends MongoRepository<Cargo, String> {
    Cargo findById(ObjectId id);
}

package com.ub.nextcargo.persistance;

import com.ub.nextcargo.domen.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, String> {
}

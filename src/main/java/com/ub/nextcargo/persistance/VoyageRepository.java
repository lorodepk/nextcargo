package com.ub.nextcargo.persistance;

import com.ub.nextcargo.domen.CarrierMovement;
import com.ub.nextcargo.domen.Schedule;
import com.ub.nextcargo.domen.Voyage;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface VoyageRepository extends MongoRepository<Voyage, String> {
}

package com.ub.nextcargo.domen;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public final class CarrierMovement {
    private Location departureLocation;
    private Location arriveLocation;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime departureTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime arrivalTime;


    public CarrierMovement(){
        departureLocation = new Location("null");
        arriveLocation = new Location("null");
        departureTime = LocalDateTime.of(1969,1,1,1,1);
        arrivalTime = LocalDateTime.of(1969,1,1,1,1);
    }

    public CarrierMovement(Location departureLocation, Location arriveLocation, LocalDateTime departureTime, LocalDateTime arrivalTime){
        this.departureLocation = departureLocation;
        this.arriveLocation = arriveLocation;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public Location getArriveLocation() {
        return arriveLocation;
    }

    public Location getDepartureLocation() {
        return departureLocation;
    }
}
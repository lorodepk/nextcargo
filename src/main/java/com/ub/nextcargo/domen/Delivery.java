package com.ub.nextcargo.domen;

import java.time.LocalTime;

public final class Delivery {
    private Voyage currentVoyage;

    private Location lastKnownLocation;

    private enum transportationStatus{
        NOT_RECEIVED,
        IN_PORT,
        ON_BOARD,
        CLAIMED,
        UNKNOWN
    }

    private enum routingStatus{
        NOT_ROUTED,
        ROUTED,
        MISROUTED
    }

    private routingStatus RoutingStatus;
    private transportationStatus TransportationStatus;


    private LocalTime calculatedAt;

    public Delivery(){}

    public Delivery(Itinerary itinerary, Location origin, Location destination){
        RoutingStatus = routingStatus.NOT_ROUTED;
        TransportationStatus = transportationStatus.NOT_RECEIVED;
        this.calculatedAt = LocalTime.now();
    }
}

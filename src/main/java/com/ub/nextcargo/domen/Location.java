package com.ub.nextcargo.domen;

public class Location {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location(String name){
        this.name = name;
    }
}

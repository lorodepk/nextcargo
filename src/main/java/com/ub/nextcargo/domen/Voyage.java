package com.ub.nextcargo.domen;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Document
public class Voyage {
    @Id
    private ObjectId id;

    private int number = 0;
    private Schedule schedule = new Schedule();


    public Schedule getSchedule() {
        return schedule;
    }

    public ObjectId getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
package com.ub.nextcargo.domen;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public final class Itinerary {
    private List<RouteStage>routeStages = new ArrayList<>();

    public void newRouteStage(Location loadLocation, Location unloadLocation, LocalDateTime loadTime, LocalDateTime unloadTime, int voyageNumber){
        routeStages.add(new RouteStage(loadLocation,unloadLocation,loadTime,unloadTime,voyageNumber));
    }

    public List<RouteStage> getRouteStages() {
        return routeStages;
    }
}

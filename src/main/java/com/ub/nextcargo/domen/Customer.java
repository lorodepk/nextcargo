package com.ub.nextcargo.domen;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document
public class Customer {
    @Id
    private ObjectId id;

    private String lastName;
    private String firstName;
    private String middleName;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate birthDay;

    public Customer(String lastName,String firstName, String middleName,LocalDate birthDay){
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthDay = birthDay;
    }



    public LocalDate getBirthDay() {
        return birthDay;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ObjectId getId() {
        return id;
    }
}

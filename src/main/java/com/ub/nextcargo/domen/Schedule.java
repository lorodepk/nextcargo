package com.ub.nextcargo.domen;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public final class Schedule {
    private List<CarrierMovement>carrierMovements = new ArrayList<>();


    public void newCarrierMovement(Location departureLocation, Location arriveLocation, LocalDateTime departureTime, LocalDateTime arrivalTime){
        carrierMovements.add(new CarrierMovement(departureLocation,arriveLocation,departureTime,arrivalTime));
    }

    public List<CarrierMovement> getCarrierMovements() {
        return carrierMovements;
    }
    public int getSize(){return carrierMovements.size();}
}

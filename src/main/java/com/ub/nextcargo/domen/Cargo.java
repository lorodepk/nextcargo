package com.ub.nextcargo.domen;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document
public class Cargo {
    @Id
    private ObjectId id;

    private ObjectId CustomerId;

    private Location origin;
    private Location destination;

    private LocalDate originDate;
    private LocalDate destinationDate;


    private Itinerary itinerary = new Itinerary();
    private Delivery delivery = new Delivery();

    public Cargo(){}

    public Cargo(Location origin,Location destination, LocalDate originDate, LocalDate destinationDate,Itinerary itinerary){
        if(origin != null && destination != null && originDate != null && destinationDate != null && origin != destination){
            this.origin = origin;
            this.destination = destination;
            this.originDate = originDate;
            this.destinationDate = destinationDate;
            this.itinerary = itinerary;
            delivery = new Delivery(itinerary,origin,destination);
        }
    }

    public ObjectId getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(ObjectId customerId) {
        CustomerId = customerId;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setItinerary(Itinerary itinerary) {
        this.itinerary = itinerary;
    }

    public ObjectId getId() {
        return id;
    }

    public Itinerary getItinerary(){
        return itinerary;
    }
}

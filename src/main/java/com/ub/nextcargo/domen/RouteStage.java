package com.ub.nextcargo.domen;

import java.time.LocalDateTime;

public final class RouteStage {
    private int voyageNumber;

    private Location loadLocation;
    private Location unloadLocation;

    private LocalDateTime loadTime;
    private LocalDateTime unloadTime;

    public RouteStage(Location loadLocation, Location unloadLocation, LocalDateTime loadTime, LocalDateTime unloadTime,int voyageNumber){
        this.voyageNumber = voyageNumber;
        this.loadLocation = loadLocation;
        this.unloadLocation = unloadLocation;
        this.loadTime = loadTime;
        this.unloadTime = unloadTime;
    }

    public LocalDateTime getUnloadTime() {
        return unloadTime;
    }

    public LocalDateTime getLoadTime() {
        return loadTime;
    }

    public Location getUnloadLocation() {
        return unloadLocation;
    }

    public Location getLoadLocation() {
        return loadLocation;
    }

    public int getVoyageNumber() {
        return voyageNumber;
    }
}

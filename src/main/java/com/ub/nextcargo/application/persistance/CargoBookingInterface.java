package com.ub.nextcargo.application.persistance;

import com.ub.nextcargo.domen.*;
import org.bson.types.ObjectId;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface CargoBookingInterface {
    public void addSchedule(Location departureLocation, Location arriveLocation, LocalDateTime departureTime, LocalDateTime arrivalTime); // Добавление новых маршрутов в расписание
    public void setScheduleForVoyage(); // Передача расписания рейса
    public void saveVoyage(); // Сохранение рейса в базу данных и создание новых объектов "рейс","расписание рейса"
    public List<Voyage> getVoyageRepository();  // Вывод рейса из БД методом findAll
    public void inputCustomer(String lastName, String firstName, String middleName, LocalDate birthDay);
    public void generateItinerary(Location origin, Location destination);  // Постороение плана маршрута
    public List<CarrierMovement> getShortestPath(); // Вывод всех возможных маршрутов.
    public void clearShortestPath(); // Отчистка списка
    public void setItinerary(int numberItinerary); // Выбор плана маршрута с последующим сохранением в БД
    public List<Cargo> getCargoRepository(); // Вывод информации о всех грузах из БД
    public Cargo getCargoFromId(ObjectId id); // Вывод информации о конкретном грузе по id
    public void newItinerary(ObjectId id,Location destination); // Смена места назначения
    public Schedule getSchedule();
}

package com.ub.nextcargo.application;

import com.ub.nextcargo.application.persistance.CargoBookingInterface;
import com.ub.nextcargo.domen.*;
import com.ub.nextcargo.persistance.CargoRepository;
import com.ub.nextcargo.persistance.CustomerRepository;
import com.ub.nextcargo.persistance.VoyageRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CargoBookingService implements CargoBookingInterface {
    @Autowired
    private VoyageRepository voyageRepository;
    @Autowired
    private CargoRepository cargoRepository;
    @Autowired
    CustomerRepository customerRepository;

    private Voyage voyage = new Voyage();
    private Schedule schedule = new Schedule();
    private Itinerary itinerary = new Itinerary();
    private List<CarrierMovement> shortestPath = new ArrayList<>();
    private Cargo cargo;
    private Customer customer;
    private Location origin;
    private Location destination;
    private boolean cargoUpdate = false;

    public Schedule getSchedule() {
        return schedule;
    }

    public void addSchedule(Location departureLocation, Location arriveLocation, LocalDateTime departureTime, LocalDateTime arrivalTime){
        schedule.newCarrierMovement(departureLocation,arriveLocation,departureTime,arrivalTime);
    }
    public void setScheduleForVoyage(){
        voyage.setSchedule(schedule);
    }
    public void saveVoyage(){
        if(voyageRepository.findAll().isEmpty()){
            voyage.setNumber(voyage.getNumber()+1);
        }else {
            voyage.setNumber(voyageRepository.findAll().get(voyageRepository.findAll().size()-1).getNumber() + 1);
        }
        voyage.setSchedule(schedule);
        voyageRepository.save(voyage);
        voyage = new Voyage();
        schedule = new Schedule();
    }
    public List<Voyage> getVoyageRepository(){
        List<Voyage>voyages = voyageRepository.findAll();
        return voyages;
    }

    public void inputCustomer(String lastName, String firstName, String middleName, LocalDate birthDay){
        customer = new Customer(lastName,firstName,middleName,birthDay);
    }

    public void generateItinerary(Location origin, Location destination){
        if(origin != null && destination != null && origin != destination) {
            List<CarrierMovement> origins = new ArrayList<>();
            List<CarrierMovement> destinations = new ArrayList<>();
            List<CarrierMovement> remain = new ArrayList<>();
            this.origin = new Location(origin.getName());
            this.destination = new Location(destination.getName());

            for (Voyage voyage : getVoyageRepository()) {
                for (CarrierMovement carrierMovement : voyage.getSchedule().getCarrierMovements()) {
                    if (carrierMovement.getDepartureLocation().getName().equals(origin.getName())) {
                        if (carrierMovement.getArriveLocation().getName().equals(destination.getName())) {
                            shortestPath.add(carrierMovement);
                        }else{
                            origins.add(carrierMovement);
                        }
                    } else if (carrierMovement.getArriveLocation().getName().equals(destination.getName())) {
                        destinations.add(carrierMovement);
                    } else{
                        remain.add(carrierMovement);
                    }
                }
            }

            for(CarrierMovement carrierMovementOrigin : origins){
                for (CarrierMovement carrierMovementDestination : destinations){
                    if(carrierMovementOrigin.getArriveLocation().getName().equals(carrierMovementDestination.getDepartureLocation().getName())
                            && carrierMovementOrigin.getArrivalTime().isBefore(carrierMovementDestination.getDepartureTime())){
                        shortestPath.add(carrierMovementOrigin);
                        shortestPath.add(carrierMovementDestination);
                    }
                }
            }

            CarrierMovement saveOriginBranch = new CarrierMovement();
            CarrierMovement saveDestinationBranch = new CarrierMovement();

            for(CarrierMovement carrierMovementBranch : remain){
                boolean saveCheck3 = true;
                boolean saveCheck4 = true;
                for(CarrierMovement carrierMovementOrigin: origins){
                    for (CarrierMovement carrierMovementDestination : destinations){
                        if(carrierMovementOrigin.getArriveLocation().getName().equals(carrierMovementBranch.getDepartureLocation().getName())
                               && carrierMovementOrigin.getArrivalTime().isBefore(carrierMovementBranch.getDepartureTime()))
                        {
                            if(carrierMovementBranch.getArriveLocation().getName().equals(carrierMovementDestination.getDepartureLocation().getName())
                                   && carrierMovementBranch.getArrivalTime().isBefore(carrierMovementDestination.getDepartureTime())){
                                if(saveCheck3){
                                    shortestPath.add(carrierMovementOrigin);
                                    shortestPath.add(carrierMovementBranch);
                                    shortestPath.add(carrierMovementDestination);
                                    saveCheck3 = false;
                                }
                            }else{
                                saveOriginBranch = carrierMovementBranch;
                            }
                        }else if(carrierMovementBranch.getArriveLocation().getName().equals(carrierMovementDestination.getDepartureLocation().getName())
                                && carrierMovementBranch.getArrivalTime().isBefore(carrierMovementDestination.getDepartureTime())){
                            saveDestinationBranch = carrierMovementBranch;
                        }

                        if(saveOriginBranch.getArriveLocation().getName().equals(saveDestinationBranch.getDepartureLocation().getName())
                                && saveOriginBranch.getArrivalTime().isBefore(saveDestinationBranch.getDepartureTime())){
                            if(saveCheck4){
                                shortestPath.add(carrierMovementOrigin);
                                shortestPath.add(saveOriginBranch);
                                shortestPath.add(saveDestinationBranch);
                                shortestPath.add(carrierMovementDestination);
                                saveCheck4 = false;
                            }
                        }
                    }
                }
            }
        }
    }

    public List<CarrierMovement> getShortestPath() {
        return shortestPath;
    }

    public void clearShortestPath(){
        shortestPath.clear();
    }

    public void setItinerary(int numberItinerary){
        int counter = 0;
        boolean saveCheck = false;
        for(CarrierMovement carrierMovement : shortestPath){
            if(counter == numberItinerary){
                itinerary.newRouteStage(carrierMovement.getDepartureLocation(),carrierMovement.getArriveLocation(),carrierMovement.getDepartureTime(),carrierMovement.getArrivalTime(), 0);
                saveCheck = true;
            }
            if(carrierMovement.getArriveLocation().getName().equals(destination.getName())){
                counter++;
                if(saveCheck){
                    break;
                }
            }
        }
        if(saveCheck){
            if(cargoUpdate){
                ObjectId id = cargo.getId();
                cargo = new Cargo(origin,destination,
                        itinerary.getRouteStages().get(0).getLoadTime().toLocalDate(),
                        itinerary.getRouteStages().get(itinerary.getRouteStages().size()-1).getUnloadTime().toLocalDate(),itinerary);
                cargo.setId(id);
                cargoRepository.save(cargo);
                cargoUpdate = false;
            }
            else if(customer != null){
                cargo = new Cargo(origin,destination,
                        itinerary.getRouteStages().get(0).getLoadTime().toLocalDate(),
                        itinerary.getRouteStages().get(itinerary.getRouteStages().size()-1).getUnloadTime().toLocalDate(),itinerary);
                cargo.setCustomerId(customer.getId());
                customerRepository.save(customer);
                cargoRepository.save(cargo);
            }
        }
        customer = null;
    }

    public List<Cargo> getCargoRepository() {
        return cargoRepository.findAll();
    }

    public Cargo getCargoFromId(ObjectId id){
        return cargoRepository.findById(id);
    }

    public void newItinerary(ObjectId id,Location destination){
        for(RouteStage routeStage : cargoRepository.findById(id).getItinerary().getRouteStages()){
            if(routeStage.getUnloadTime().isAfter(LocalDateTime.now())){
                clearShortestPath();
                generateItinerary(routeStage.getUnloadLocation() , destination);
                cargo = cargoRepository.findById(id);
                cargoUpdate = true;
                break;
            }
        }
        if(!cargoUpdate){
            clearShortestPath();
            Itinerary it =  cargoRepository.findById(id).getItinerary();
            generateItinerary(it.getRouteStages().get(it.getRouteStages().size() - 1).getUnloadLocation(),destination);
            cargo = cargoRepository.findById(id);
            cargoUpdate = true;
        }
    }
}

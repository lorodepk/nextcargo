package com.ub.nextcargo.applicationController;


import com.ub.nextcargo.application.persistance.CargoBookingInterface;
import com.ub.nextcargo.domen.*;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/")
public class applicationController {
    @Autowired
    private CargoBookingInterface cargoBookingInterface;

    @GetMapping
    public List<Voyage> getVoyageRepository(){
        return cargoBookingInterface.getVoyageRepository();
    }

    @PostMapping
    public void addSchedule(@RequestBody CarrierMovement carrierMovement){
        cargoBookingInterface.addSchedule(carrierMovement.getDepartureLocation(),carrierMovement.getArriveLocation(),carrierMovement.getDepartureTime(),carrierMovement.getArrivalTime());
    }

    @GetMapping("/save")
    public void saveVoyage(){
        cargoBookingInterface.saveVoyage();
    }

    @GetMapping("/generateItinerary")
    public void openGenerateItinerary(){
    }

    @PostMapping("/generateItinerary")
    public void generateItinerary(@RequestBody Map<String,String> params){
        cargoBookingInterface.inputCustomer(params.get("firstName"),params.get("lastName"),params.get("middleName"), LocalDate.parse(params.get("birthDay")));
        cargoBookingInterface.generateItinerary(new Location(params.get("origin")), new Location(params.get("destination")));
    }

    @GetMapping("/showItinerary")
    public List<CarrierMovement> showItinerary(){
        return cargoBookingInterface.getShortestPath();
    }

    @PostMapping("/showItinerary")
    public void setItinerary(@RequestBody int numberItinerary){
        cargoBookingInterface.setItinerary(numberItinerary);
        cargoBookingInterface.clearShortestPath();
    }

    @GetMapping("/showCargo")
    public List<Cargo> showCargo(){
        return cargoBookingInterface.getCargoRepository();
    }

    @GetMapping("/cargo/{id}")
    public Cargo showOneCargo(@PathVariable ObjectId id){
        return cargoBookingInterface.getCargoFromId(id);
    }

    @PostMapping("/cargo/{id}")
    public void newItinerary(@RequestBody Location destination,@PathVariable ObjectId id){
        cargoBookingInterface.newItinerary(id,destination);
    }
}
